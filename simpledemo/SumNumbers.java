
public class SumNumbers {
	
	public SumNumbers() {}
	
	public int sumNumberRange(int lowerNumber, int higherNumber) {
		int total = 0;
		
		for(int i = lowerNumber; i <= higherNumber; i++) {
			total += i;
		}
		return total;
	} // end of method
	
	public static void main(String[] args) { // manual testing in main, not official
		SumNumbers s = new SumNumbers();
		int result = s.sumNumberRange(3, 5);
		System.out.println("Result is: " + result);
	} // end of main
	
} // end of class


