
import static org.junit.Assert.assertEquals;
import org.junit.Test;


public class SumNumbersTest {

	@Test
	public void testOrdered() {
		SumNumbers s = new SumNumbers();
		assertEquals(s.sumNumberRange(3, 5), 12);
		assertEquals(s.sumNumberRange(1, 4), 10);
	}
	
	@Test
	public void testEqual() {
		SumNumbers s = new SumNumbers();
		assertEquals(s.sumNumberRange(5, 5), 5);
	}
	
	@Test
	public void testReversed() {
		SumNumbers s = new SumNumbers();
		assertEquals(s.sumNumberRange(10, 5), 0);
	}
	
	@Test
	public void testNegative() {
		SumNumbers s = new SumNumbers();
		assertEquals(s.sumNumberRange(-1, 4), 9);
	}

}
